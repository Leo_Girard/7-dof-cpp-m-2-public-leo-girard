#include "kinematic_model.h"
#include <iostream>

int main(){  
    Eigen::Vector7d q(M_PI/3.0, M_PI_4, 0.0, -M_PI_2, 0.0, -M_PI/3.0, M_PI_2);
    Eigen::Affine3d X;
    Eigen::Matrix67d J;
    std::array<double,8> a = {0.0, 0.0, 0.0, 0.0825, -0.0825, 0.0, 0.088, 0.0};
    std::array<double,8> d = {0.333, 0.0, 0.316, 0.0, 0.384, 0.0, 0.0, 0.107};
    std::array<double,8> alpha = {0.0, -M_PI_2, M_PI_2, M_PI_2, -M_PI_2, M_PI_2, M_PI_2, 0.0};

    RobotModel Marybell(a,d,alpha);

    Marybell.FwdKin(X,J,q);
    std::cout<< "J = "<< J <<std::endl;
    std::cout<< "X.t = "<< X.translation() <<std::endl;
    std::cout<< "X.r = "<< X.rotation() <<std::endl<<std::endl;
}
