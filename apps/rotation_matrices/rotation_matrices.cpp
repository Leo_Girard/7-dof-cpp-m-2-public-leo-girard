#include <iostream>
#include "eigen3/Eigen/Dense"

int main(){  
  system("clear");
  // See reminder in "Variation Between Two Rotation Matrices.pdf" 
  Eigen::AngleAxisd aar     { Eigen::AngleAxisd ::Identity()    };
  Eigen::Matrix3d   R       { Eigen::Matrix3d   ::Zero()        };
  Eigen::Matrix3d   R2      { Eigen::Matrix3d   ::Zero()        };
  Eigen::Matrix3d   E       { Eigen::Matrix3d   ::Zero()        };
  Eigen::Matrix3d   E1      { Eigen::Matrix3d   ::Zero()        };
  Eigen::Matrix3d   E2      { Eigen::Matrix3d   ::Zero()        };
  Eigen::Matrix3d   E2t     { Eigen::Matrix3d   ::Zero()        };
  
  Eigen::Vector3d   v1      { Eigen::Vector3d   ::Random()      };
  Eigen::Vector3d   v2      { Eigen::Vector3d   ::Random()      };
  
  v1.normalize();
  v2.normalize();
  E         = Eigen::AngleAxisd(M_PI/3.,v1).matrix();
  aar       = Eigen::AngleAxisd(M_PI/4.,v2);
  R         = aar.matrix();
  E1        = R*E;                                            // left  multiplication
  E2        = E*R;                                            // right multiplication
  R2        = Eigen::AngleAxisd(aar.angle(),E * aar.axis());  // 
  E2t       = R2 * E;                                         // left  multiplication equivalent to E*R
  
  std::cout << "E2        = \n" << E2 << "\n\n";
  std::cout << "E2t       = \n" << E2t << "\n\n";
  std::cout << "E2 - E2t  = \n" << E2 - E2t << "\n\n";
}  
