#include "trajectory_generation.h"
#include <iostream>
#include <fstream>
using namespace std;

int main(){  
Eigen::Affine3d X_i,X_f,X;
Eigen::Matrix3d Id,R;
Eigen::Vector3d P,TrueV;
Eigen::Vector6d V;
Eigen::AngleAxisd rf,rota;

Eigen::Vector3d last_V(0,0,0); // memory vector for preceding position

Eigen::Vector3d Vector(1,5,7); // axis for representation axis/angle of desired rotation

Vector.normalize(); // normalization of axis in order to compute correctly

Id = Eigen::Matrix3d::Identity();
rf = Eigen::AngleAxisd(M_PI/2, Vector); // computation of final rotation matrix

X_i.translation() = Eigen::Vector3d(0,0,0) ; // initialsation of initial pose
X_i.linear() = Id;

X_f.translation() = Eigen::Vector3d(1,3,9); // initialization of final pose
X_f.linear() =  rf.matrix();

double tf = 3; // init of final time


Point2Point Test_traj(X_i,X_f,tf); // creation of object trajectory

ofstream myfile;
myfile.open("output.csv"); // .csv file to see if we can attain desired position and also plot velocity
myfile << "Vx"<<","<< "Vy"<<","<< "Vz" <<","<< "TVx" <<","<< "TVy" <<","<< "Tvz" <<","<< "t" <<"\n";
double t =0.0;

while(t<tf)
{
    X = Test_traj.X(t); // computation of current pose 
    V = Test_traj.dX(t); // computation of current velocity

    P = X.translation(); // extraction of translation
    R = X.rotation(); // extraction of rotation
   // rota = X.rotation();

    cout<< "P = "<< P <<endl;
    cout<< "R = "<< R <<endl;
    cout<< "Rf = "<< rf.matrix() <<endl<<endl;

    TrueV = (P-last_V)/0.01; // computation of true speed
    //TrueV = (P-last_V)/0.01;

    cout<< "V ="<<V <<endl<<endl;
    //Angle = rota.axis();
    myfile << V(0) <<","<< V(1) <<","<< V(2) <<","<< TrueV[0] <<","<< TrueV[1] <<","<< TrueV[2]<<","<< t <<"\n";
    t+=0.01;
    last_V = P; //memorisation of translation
}
myfile.close();

}