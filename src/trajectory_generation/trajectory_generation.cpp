#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  double D = pfIn - piIn; // computing the diference between final and initial position;
  pi = piIn;
  pf = pfIn;

  // Computing polynomial coefficients
  a[0] = piIn; 
  a[1] = 0;
  a[2] = 0;
  a[3] = (10 / pow(DtIn, 3)) * D;
  a[4] = (-15 / pow(DtIn,4)) * D;
  a[5] = (6 / pow(DtIn,5)) * D;
 
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  double D = pfIn - piIn; // computing the diference between final and initial position;
  
  // Computing polynomial coefficients
  a[0] = piIn; 
  a[1] = 0;
  a[2] = 0;
  a[3] = (10 / pow(DtIn, 3)) * D;
  a[4] = (-15 / pow(DtIn,4)) * D;
  a[5] = (6 / pow(DtIn,5)) * D;
};

const double  Polynomial::p     (const double &t){
    double pd;

  pd =  a[0]+ a[1]*t + a[2]*pow(t,2) + a[3]*pow(t,3) + a[4]*pow(t,4) + a[5]*pow(t,5); // computing the next position of trajectory
  return pd;
  //have to add angles there ?

};

const double  Polynomial::dp    (const double &t){
  double vd;

  vd = a[1] + 2*a[2]*t + 3*a[3]*pow(t,2) + 4*a[4]*pow(t,3) + 5*a[5]*pow(t,4); // computing velocity by the derivative of the position
  return vd;
};

Point2Point::Point2Point(const Eigen::Affine3d & X_i, const Eigen::Affine3d & X_f, const double & DtIn){
  Eigen::Vector3d Ti = X_i.translation(); //affine = homogenous matrix -> extraction of translation part
  Eigen::Matrix3d Ri = X_i.rotation(); // extraction of rotation part

  Eigen::Vector3d Tf = X_f.translation();
  Eigen::Matrix3d Rf = X_f.rotation();

  polx.update(Ti[0],Tf[0],DtIn); 
  poly.update(Ti[1],Tf[1],DtIn); 
  polz.update(Ti[2],Tf[2],DtIn);

  // for rotation axis use : rotaa = (new_rotation matrix)
  R0 = Rf * Ri.transpose();
  rot_aa = R0;
  axis = rot_aa.axis();

  pol_angle.update(0,rot_aa.angle(),DtIn);

  Dt = DtIn;

}

Eigen::Affine3d Point2Point::X(const double & time){
  Eigen::Affine3d Xd;
  
  Xd.translation()(0) = polx.p(time);
  Xd.translation()(1) = poly.p(time);
  Xd.translation()(2) = polz.p(time);
  
  Eigen::AngleAxisd Rot(pol_angle.p(time),axis);

  Xd.linear() = Rot.matrix(); // .linear = .rotation that can be written over

  return Xd;
}

Eigen::Vector6d Point2Point::dX(const double & time){
  Eigen::Vector6d dXd;

  dXd[0] = polx.dp(time);
  dXd[1] = poly.dp(time);
  dXd[2] = polz.dp(time);
  dXd[3] = pol_angle.dp(time)*axis[0];
  dXd[4] = pol_angle.dp(time)*axis[1];
  dXd[5] = pol_angle.dp(time)*axis[2];
  
  return dXd;
}