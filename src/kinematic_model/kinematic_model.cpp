#include "kinematic_model.h"
#include <iostream>
RobotModel::RobotModel(const std::array<double,8> &aIn, const std::array<double,8> &dIn, const std::array<double,8> &alphaIn):
  a     {aIn},
  d     {dIn},
  alpha {alphaIn}
{ 
};

void RobotModel::FwdKin(Eigen::Affine3d &xOut, Eigen::Matrix67d &JOut, const Eigen::Vector7d & qIn){
  Eigen::Affine3d T_temp = Eigen::Affine3d::Identity(); // need to save every computed p and z from each transformation matrix
  Eigen::Affine3d o_T_1, o_T_2, o_T_3, o_T_4, o_T_5, o_T_6, o_T_7; // homogenous transformation matrixes
  Eigen::Vector3d o_z_1, o_z_2, o_z_3, o_z_4, o_z_5, o_z_6, o_z_7; // z vectors for each 0_T_j transformation matrix
  Eigen::Vector3d o_p_1, o_p_2, o_p_3, o_p_4, o_p_5, o_p_6, o_p_7; // p vectors for each 0_T_j transformation matrix
  Eigen::Matrix67d J;
  
  //xOut = 
  for(int j = 0; j<7; j++)
  {
    //computation of j-1Tj matix  //we went with the assumption that a corresponds to the a in the course and d is the equivalrnt of r 
    T_temp(0,0) = cos(qIn[j]);
    T_temp(0,1) = -sin(qIn[j]);
    T_temp(0,2) = 0;
    T_temp(0,3) = a[j];

    T_temp(1,0) = cos(alpha[j]) * sin(qIn[j]);
    T_temp(1,1) = cos(alpha[j]) * cos(qIn[j]);
    T_temp(1,2) = -sin(alpha[j]);
    T_temp(1,3) = -sin(alpha[j]) * d[j]; //here dj == rj (since Panda has no prismatic joint using a constant to represent r is not an issue)

    T_temp(2,0) = sin(alpha[j]) * sin(qIn[j]);
    T_temp(2,1) = sin(alpha[j]) * cos(qIn[j]);
    T_temp(2,2) = cos(alpha[j]);
    T_temp(2,3) = cos(alpha[j]) * d[j];

    T_temp(3,0) = 0;
    T_temp(3,1) = 0;
    T_temp(3,2) = 0;
    T_temp(3,3) = 1;

    switch(j) // savepoint for the T matrixes
    {
      case 0: // 0T1 
        o_T_1 = T_temp;
        o_p_1 = o_T_1.translation();
        o_z_1[0] = o_T_1.rotation()(0,2);
        o_z_1[1] = o_T_1.rotation()(1,2);
        o_z_1[2] = o_T_1.rotation()(2,2);
        break;
      
      case 1: // 0T2
        o_T_2 = o_T_1 * T_temp;
        o_p_2 = o_T_2.translation();
        o_z_2[0] = o_T_2.rotation()(0,2);
        o_z_2[1] = o_T_2.rotation()(1,2);
        o_z_2[2] = o_T_2.rotation()(2,2);
        break;
      
      case 2: // 0T3
        o_T_3 = o_T_2 * T_temp;
        o_p_3 = o_T_3.translation();
        o_z_3[0] = o_T_3.rotation()(0,2);
        o_z_3[1] = o_T_3.rotation()(1,2);
        o_z_3[2] = o_T_3.rotation()(2,2);
        break;

      case 3: // 0T4
        o_T_4 = o_T_3 * T_temp;
        o_p_4 = o_T_4.translation();
        o_z_4[0] = o_T_4.rotation()(0,2);
        o_z_4[1] = o_T_4.rotation()(1,2);
        o_z_4[2] = o_T_4.rotation()(2,2);
        break;
      
      case 4: // 0T5
        o_T_5 = o_T_4 * T_temp;
        o_p_5 = o_T_5.translation();
        o_z_5[0] = o_T_5.rotation()(0,2);
        o_z_5[1] = o_T_5.rotation()(1,2);
        o_z_5[2] = o_T_5.rotation()(2,2);
        break;

      case 5: //0T6
        o_T_6 = o_T_5 * T_temp;
        o_p_6 = o_T_6.translation();
        o_z_6[0] = o_T_6.rotation()(0,2);
        o_z_6[1] = o_T_6.rotation()(1,2);
        o_z_6[2] = o_T_6.rotation()(2,2);
        break;

      case 6: //0T7
        o_T_7 = o_T_6 * T_temp;
        o_p_7 = o_T_7.translation();
        o_z_7[0] = o_T_7.rotation()(0,2);
        o_z_7[1] = o_T_7.rotation()(1,2);
        o_z_7[2] = o_T_7.rotation()(2,2);
        break;

      default:
        std::cout<< "error invalid loop variable"<<std::endl;
        break;
    }
  }
  // j-1_z_j is the vector corrsponding to j-1_T_j(0-2,2) and j-1_p_j is the vector corrsponding to j-1_T_j(0-2,3)
  
  //Computation of X
  //position computation
  Eigen::Vector4d s_O_8 (a[7],0,d[8],1);
  //Eigen::Matrix41d O_8_temp;
  //Eigen::Vector3d o_O_8;

  auto O_8_temp = o_T_7 * s_O_8;

  xOut.translation()(0) = O_8_temp(0,0);
  xOut.translation()(1) = O_8_temp(1,0);
  xOut.translation()(2) = O_8_temp(2,0);
  /*xOut[1] = O_8_temp(1,0);
  xOut[2] = O_8_temp(2,0);*/

  //Computation of orientation
  //Eigen::Matrix3d o_R_7;
  xOut.linear()= o_T_7.rotation();
  
  // Computation of jacobian matrix
  J.block(0,0,3,1) = o_z_1.cross(o_p_7 - o_p_1);
  J.block(3,0,3,1) = o_z_1; 

  J.block(0,1,3,1) = o_z_2.cross(o_p_7 - o_p_2);
  J.block(3,1,3,1) = o_z_2;

  J.block(0,2,3,1) = o_z_3.cross(o_p_7 - o_p_3);
  J.block(3,2,3,1) = o_z_3;

  J.block(0,3,3,1) = o_z_4.cross(o_p_7 - o_p_4);
  J.block(3,3,3,1) = o_z_4;

  J.block(0,4,3,1) = o_z_5.cross(o_p_7 - o_p_5);
  J.block(3,4,3,1) = o_z_5;

  J.block(0,5,3,1) = o_z_6.cross(o_p_7 - o_p_6);
  J.block(3,5,3,1) = o_z_6;

  J.block(0,6,3,1) = o_z_7.cross(o_p_7 - o_p_7);
  J.block(3,6,3,1) = o_z_7;

  Eigen::Matrix3d C = Eigen::Matrix3d::Identity();
  Eigen::Matrix3d I = Eigen::Matrix3d::Identity();
  Eigen::Matrix3d O = Eigen::Matrix3d::Zero();
  Eigen::Matrix3d D;
  //Eigen::Matrix6d M;
  Eigen::MatrixXd M(6,6);
  double xx,xy,xz,yx,yy,yz,zx,zy,zz;

  xx = o_T_7.rotation()(0,0);
  xy = o_T_7.rotation()(1,0);
  xz = o_T_7.rotation()(2,0);

  yx = o_T_7.rotation()(0,1);
  yy = o_T_7.rotation()(1,1);
  yz = o_T_7.rotation()(2,1);

  zx = o_T_7.rotation()(0,2);
  zy = o_T_7.rotation()(1,2);
  zz = o_T_7.rotation()(2,2);

  D(0,0) = 0;
  D(1,0) = -a[7]*xz - d[8]*zz;
  D(2,0) = a[7]*xy + d[8]*zy;
    
  D(0,1) = a[7]*xz + d[8]*zz;
  D(1,1) = 0;
  D(2,1) = -a[7]*xx - d[8]*zx;
    
  D(0,2) = -a[7]*xy - d[8]*zy;
  D(1,2) = a[7]*xx + d[8]*zx;
  D(2,2) = 0;

  M << I,D,O,C;

  JOut = M * J;

}